#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"
  # Extra ATLAS-KV-specific setup
  # (extracted from source /code/resources/env.sh 17.8.0.9,AtlasProduction)
  export ATLAS_LOCAL_ROOT_BASE="/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase"
  source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh -q
  source $AtlasSetup/scripts/asetup.sh 17.8.0.9,AtlasProduction
  # Configure WL copy
  cp $BMKDIR/preInclude.SingleMuonGenerator.py .
  cp -r $BMKDIR/sqlite200 .
  # Execute WL copy
  /cvmfs/atlas.cern.ch/repo/sw/software/x86_64-slc6-gcc46-opt/17.8.0/AtlasProduction/17.8.0.9/InstallArea/share/bin/AtlasG4_trf.py outputHitsFile='KitValidation-SimulHITS-17.8.0.9.pool.root' maxEvents="$NEVENTS_THREAD" skipEvents='0' preInclude='KitValidation/kv_reflex.py,preInclude.SingleMuonGenerator.py' geometryVersion='ATLAS-GEO-16-00-00' conditionsTag='OFLCOND-SDR-BS7T-04-03' >out_$1.log 2>&1
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NCOPIES=$(nproc)
NTHREADS=1 # cannot be changed by user input (single-threaded single-process WL)
NEVENTS_THREAD=100

# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  echo "!!!! Local bmk-driver.sh !!!!"
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
