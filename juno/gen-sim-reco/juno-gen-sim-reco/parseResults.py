#!/usr/bin/python

import sys
import os
import json
from typing import DefaultDict 

#print("########################PARSERESULTS.PY########################")

GenScores = []
SimScores = []
TrigSimScores = []
RecoScores = []
TotalScores = []

MaxGen = 0
AvgGen = 0
MedGen = 0
MinGen = 1000000

MaxSim = 0
AvgSim = 0
MedSim = 0
MinSim = 1000000

MaxTrigSim = 0
AvgTrigSim = 0
MedTrigSim = 0
MinTrigSim = 1000000

MaxReco = 0
AvgReco = 0
MedReco = 0
MinReco = 1000000

MaxTotal = 0
AvgTotal = 0
MedTotal = 0
MinTotal = 1000000

def event_time(filename):
	try:
		print("Opening log file %s" % filename)
		file = open(filename,'r')
	except IOError:
        	error = []
	        return error
	content = file.read()
	file.close()
	return float(content.strip())

 
# Each copy generates an output file
for i in range(0, int(os.environ['NCOPIES'])):
#for i in range(0,1):
	simtime = "proc_" + str(i + 1) + "/outputsim.time"
	deteletime = "proc_" + str(i + 1) + "/outputdetele.time"
	elecaltime = "proc_" + str(i + 1) + "/outputelecal.time"
	rectime = "proc_" + str(i + 1) + "/outputrec.time"

	# The parser reports separately the time taken by event generation, simulation, trigger simulation, 
	# and reconstruction (and the total). Times are reported from many sub-modules; these keywords 
	# are used to make the groupings
	#keywords = ["EvtGenInput", "Sum_Simulation", "Sum_TriggerSimulation", "SoftwareTrigger"]
	#keepgoing = False;
	#startintwo = False;

	Gen = 0
	Sim = 0
	TrigSim = 0
	Reco = 0
	Total = 0

	Gen = event_time(simtime)
	Sim = event_time(deteletime)
	TrigSim = event_time(elecaltime)
	Reco = event_time(rectime)
	Total = Gen + Sim + TrigSim + Reco

	# Output is in seconds; convert to events / second
	Gen = float(os.environ['NEVENTS_THREAD']) / Gen
	Sim = float(os.environ['NEVENTS_THREAD']) / Sim
	TrigSim = float(os.environ['NEVENTS_THREAD']) / TrigSim
	Reco = float(os.environ['NEVENTS_THREAD']) / Reco
	Total = float(os.environ['NEVENTS_THREAD']) / Total

	print ("Gen: " + str(Gen))
	print ("Sim: " + str(Sim))
	print ("TrigSim: " + str(TrigSim))
	print ("Reco: " + str(Reco))
	print ("Total: " + str(Total))

	GenScores.append(Gen)
	SimScores.append(Sim)
	TrigSimScores.append(TrigSim)
	RecoScores.append(Reco)
	TotalScores.append(Total)

	print (GenScores)
	print (SimScores)
	print (TrigSimScores)
	print (RecoScores)
	print (TotalScores)

	if Gen > MaxGen:
		MaxGen = Gen
	if Sim > MaxSim:
		MaxSim = Sim
	if TrigSim > MaxTrigSim:
		MaxTrigSim = TrigSim
	if Reco > MaxReco:
		MaxReco = Reco
	if Total > MaxTotal:
		MaxTotal = Total

	if Gen < MinGen:
		MinGen = Gen
	if Sim < MinSim:
		MinSim = Sim
	if TrigSim < MinTrigSim:
		MinTrigSim = TrigSim
	if Reco < MinReco:
		MinReco = Reco
	if Total < MinTotal:
		MinTotal = Total

	AvgGen = AvgGen + Gen / float(os.environ['NCOPIES'])
	AvgSim = AvgSim + Sim / float(os.environ['NCOPIES'])
	AvgTrigSim = AvgTrigSim + TrigSim / float(os.environ['NCOPIES'])
	AvgReco = AvgReco + Reco / float(os.environ['NCOPIES'])
	AvgTotal = AvgTotal + Total / float(os.environ['NCOPIES'])


GenScores.sort()
SimScores.sort()
TrigSimScores.sort()
RecoScores.sort()
TotalScores.sort()

if len(GenScores) % 2 == 0:
	MedGen = (GenScores[int(len(GenScores) / 2)] + GenScores[int(len(GenScores) / 2 - 1)]) / 2.0
	MedSim = (SimScores[int(len(SimScores) / 2)] + SimScores[int(len(SimScores) / 2 - 1)]) / 2.0
	MedTrigSim = (TrigSimScores[int(len(TrigSimScores) / 2)] + TrigSimScores[int(len(TrigSimScores) / 2 - 1)]) / 2.0
	MedReco = (RecoScores[int(len(RecoScores) / 2)] + RecoScores[int(len(RecoScores) / 2 - 1)]) / 2.0
	MedTotal = (TotalScores[int(len(TotalScores) / 2)] + TotalScores[int(len(TotalScores) / 2 - 1)]) / 2.0
else:
	MedGen = GenScores[int(len(GenScores) / 2)]
	MedSim = SimScores[int(len(SimScores) / 2)]
	MedTrigSim = TrigSimScores[int(len(TrigSimScores) / 2)]
	MedReco = RecoScores[int(len(RecoScores) / 2)]
	MedTotal = TotalScores[int(len(TotalScores) / 2)]


OutputJSON = {}
OutputJSON['wl-scores'] = DefaultDict(dict)
OutputJSON['wl-stats'] =  DefaultDict(dict)

OutputJSON['wl-scores']['gen'] = AvgGen * int(os.environ['NCOPIES'])
OutputJSON['wl-scores']['sim'] = 1.0 / (1.0 / AvgSim + 1.0 / AvgTrigSim) * int(os.environ['NCOPIES'])
OutputJSON['wl-scores']['reco'] = AvgReco * int(os.environ['NCOPIES'])
OutputJSON['wl-scores']['gen-sim-reco'] = AvgTotal * int(os.environ['NCOPIES'])

OutputJSON['wl-stats']['gen-sim-reco']['avg'] = AvgTotal
#OutputJSON['wl-stats']['gen-sim-reco']['median'] = MedTotal
OutputJSON['wl-stats']['gen-sim-reco']['min'] = MinTotal
OutputJSON['wl-stats']['gen-sim-reco']['max'] = MaxTotal
OutputJSON['wl-stats']['gen-sim-reco']['count'] = int(os.environ['NCOPIES'])
OutputJSON['wl-stats']['gen']['avg'] = AvgGen
#OutputJSON['wl-stats']['gen']['median'] = MedGen
OutputJSON['wl-stats']['gen']['min'] = MinGen
OutputJSON['wl-stats']['gen']['max'] = MaxGen
OutputJSON['wl-stats']['gen']['count'] = int(os.environ['NCOPIES'])
OutputJSON['wl-stats']['sim']['avg'] = 1.0 / (1.0 / AvgSim + 1.0 / AvgTrigSim)
#OutputJSON['wl-stats']['sim']['median'] = 1.0 / (1.0 / MedSim + 1.0 / MedTrigSim)
OutputJSON['wl-stats']['sim']['min'] = 1.0 / (1.0 / MinSim + 1.0 / MinTrigSim)
OutputJSON['wl-stats']['sim']['max'] = 1.0 / (1.0 / MaxSim + 1.0 / MaxTrigSim)
OutputJSON['wl-stats']['sim']['count'] = int(os.environ['NCOPIES'])
OutputJSON['wl-stats']['reco']['avg'] = AvgReco
#OutputJSON['wl-stats']['reco']['median'] = MedReco
OutputJSON['wl-stats']['reco']['min'] = MinReco
OutputJSON['wl-stats']['reco']['max'] = MaxReco
OutputJSON['wl-stats']['reco']['count'] = int(os.environ['NCOPIES'])


OutputFile = open("parser_output.json", "w")
json.dump(OutputJSON, OutputFile)
OutputFile.close()
