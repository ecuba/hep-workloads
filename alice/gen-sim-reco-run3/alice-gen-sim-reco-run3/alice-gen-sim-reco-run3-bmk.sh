#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

#set -x # enable debug printouts

#set -e # immediate exit on error

# Function doOne must be defined in each benchmark
# Input argument $1: process index (between 1 and $NCOPIES)
# Return value: please return 0 if this workload copy was successful, 1 otherwise
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG
# The function is started in process-specific working directory <basewdir>/proc_$1:
# please store here the individual log files for each of the NCOPIES processes
function doOne(){
  if [ "$1" == "" ] || [ "$2" != "" ]; then echo "[doOne] ERROR! Invalid arguments '$@' to doOne"; return 1; fi
  echo "[doOne ($1)] $(date) starting in $(pwd)"

  #
  # A example workflow MC->RECO->AOD doing signal-background embedding, meant
  # to study embedding speedups.
  # Background events are reused across timeframes. 
  # 
  #export ALIENV_DEBUG=1
  /cvmfs/alice.cern.ch/bin/alienv printenv O2sim/v20220421-1 &> environment
  source environment
  
  export HEPSCORE_CCDB_ROOT=/cvmfs/alice.cern.ch/el7-x86_64/Packages/HEPscore-CCDB/v0.1.1-test-1
  export ROOT_INCLUDE_PATH=${ROOT_INCLUDE_PATH}:/cvmfs/alice.cern.ch/el7-x86_64/Packages/TBB/v2021.5.0-8/include

  # make sure O2DPG + O2 is loaded
  [ ! "${O2DPG_ROOT}" ] && echo "Error: This needs O2DPG loaded" && return 1
  [ ! "${O2_ROOT}" ] && echo "Error: This needs O2 loaded" && return 1

  # ----------- START ACTUAL JOB  ----------------------------- 
  NSIGEVENTS=$(($NEVENTS_THREAD * $NTHREADS))
  NBKGEVENTS=$NSIGEVENTS
  NTIMEFRAMES=2
  SIMENGINE=TGeant4 
  NWORKERS=$NTHREADS
  CPULIMIT=$NTHREADS
  MODULES="--skipModules ZDC"
  PYPROCESS=ccbar 

  # create workflow
  ${O2DPG_ROOT}/MC/bin/o2dpg_sim_workflow.py -eCM 5020 -col pp -gen pythia8 -proc ${PYPROCESS} \
                                            -colBkg PbPb -genBkg pythia8 -procBkg "heavy_ion" \
                                            -tf ${NTIMEFRAMES} -nb ${NBKGEVENTS}              \
                                            -ns ${NSIGEVENTS} -e ${SIMENGINE}                 \
                                            -j ${NWORKERS} --embedding -interactionRate 50000 \
                                            --timestamp 1635659148972 -seed 1 > out_$1.log 2>&1

  
  # copy CCDB snapshot to workdir
  cp -R ${HEPSCORE_CCDB_ROOT}/.ccdb .
  # benchmark is run with published CCDB
  echo "Setting CCDB cache to ${PWD}/.ccdb"
  export ALICEO2_CCDB_LOCALCACHE=${PWD}/.ccdb
  # fetch matbud file
  cp ${HEPSCORE_CCDB_ROOT}/data/matbud.root .
  
  # run workflow
  ${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json --cpu-limit ${CPULIMIT} -tt sgn >> out_$1.log 2>&1
  status=${?}
  echo "[doOne ($1)] $(date) completed (status=$status)"
  # Return 0 if this workload copy was successful, 1 otherwise
  return $status
}

# Optional function validateInputArguments may be defined in each benchmark
# If it exists, it is expected to set NCOPIES, NTHREADS, NEVENTS_THREAD
# (based on previous defaults and on user inputs USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS)
# Input arguments: none
# Return value: please return 0 if input arguments are valid, 1 otherwise
# The following variables are guaranteed to be defined: NCOPIES, NTHREADS, NEVENTS_THREAD
# (benchmark defaults) and USER_NCOPIES, USER_NTHREADS, USER_NEVENTS_THREADS (user inputs)
function validateInputArguments(){
  if [ "$1" != "" ]; then echo "[validateInputArguments] ERROR! Invalid arguments '$@' to validateInputArguments"; return 1; fi
  echo "[validateInputArguments] validate input arguments"
  # Number of copies and number of threads per copy
  if [ "$USER_NTHREADS" != "" ] && [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$USER_NTHREADS
  elif [ "$USER_NTHREADS" != "" ]; then
    NTHREADS=$USER_NTHREADS
    NCOPIES=$((`nproc`/$NTHREADS))
  elif [ "$USER_NCOPIES" != "" ]; then
    NCOPIES=$USER_NCOPIES
    NTHREADS=$((`nproc`/$NCOPIES))
  fi
  # Number of events per thread
  if [ "$USER_NEVENTS_THREAD" != "" ]; then NEVENTS_THREAD=$USER_NEVENTS_THREAD; fi
  # Return 0 if input arguments are valid, 1 otherwise
  return 0 # no checks
}

# Default values for NCOPIES, NTHREADS, NEVENTS_THREAD must be set in each benchmark
NTHREADS=4 
NCOPIES=$(( `nproc` / $NTHREADS ))
NEVENTS_THREAD=3
if [ "$NCOPIES" -lt 1 ]; then # when $NTHREADS > nproc
  NCOPIES=1
  NTHREADS=`nproc`
fi
# Source the common benchmark driver
if [ -f $(dirname $0)/bmk-driver.sh ]; then
  . $(dirname $0)/bmk-driver.sh
else
  . $(dirname $0)/../../../common/bmk-driver.sh
fi
