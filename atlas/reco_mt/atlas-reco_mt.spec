HEPWL_BMKEXE=atlas-reco_mt-bmk.sh 
HEPWL_BMKOPTS="-e 3 -t 4 -c1"
HEPWL_BMKDIR=atlas-reco_mt
HEPWL_BMKDESCRIPTION="Atlas data reconstruction based on release 22.0.60"
HEPWL_DOCKERIMAGENAME=atlas-reco_mt-bmk
HEPWL_DOCKERIMAGETAG=v0.1
HEPWL_CVMFSREPOS=atlas.cern.ch,atlas-condb.cern.ch,atlas-nightlies.cern.ch,sft.cern.ch
HEPWL_BMKOS="gitlab-registry.cern.ch/linuxsupport/cc7-base:20211101-1.x86_64"
