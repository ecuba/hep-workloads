#! /bin/sh
# DBRelease setup
echo Setting up DBRelease /cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/100.0.2 environment
export DBRELEASE=100.0.2
export CORAL_AUTH_PATH=/cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/100.0.2/XMLConfig
export CORAL_DBLOOKUP_PATH=/cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/100.0.2/XMLConfig
export TNS_ADMIN=/cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/100.0.2/oracle-admin
DATAPATH=/cvmfs/atlas.cern.ch/repo/sw/database/DBRelease/100.0.2:$DATAPATH
# Customised environment
athena.py --preloadlib=/cvmfs/atlas.cern.ch/repo/sw/software/22.0/AthenaExternals/22.0.27/InstallArea/x86_64-centos7-gcc8-opt/lib/libintlc.so.5:/cvmfs/atlas.cern.ch/repo/sw/software/22.0/AthenaExternals/22.0.27/InstallArea/x86_64-centos7-gcc8-opt/lib/libimf.so --threads=2 runargs.AtlasG4Tf.py SimuJobTransforms/skeleton.EVGENtoHIT_MC12.py
