# Copyright 2019-2021 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

if [ "$BASH_SOURCE" = "$0" ]; then echo "ERROR! This script ($0) was not sourced"; exit 1; fi
if [ "$BASH_SOURCE" = "" ]; then echo "ERROR! This script was not sourced from bash"; return 1; fi

parserDriver=$(basename ${BASH_SOURCE})
parserDriverDIR=$(cd $(dirname ${BASH_SOURCE}); pwd)
echo -e """
------------------------------------------------------------------------------
The script ${parserDriver} at  ${parserDriverDIR} has been sourced
------------------------------------------------------------------------------
"""

function enrich_version_json() {
  # Keep a copy of the version.json file for parser tests on previous logs
  echo "[$parserDriver: enrich_version_json]"
  if [ -f $BMKDIR/version.json ]; then
  #  cp $BMKDIR/version.json $baseWDir
  # Get container flavor at runtime & inject to version.json in parseResults
    if [ -f /run/.containerenv ]; then export flavor=podman
    elif [ -f /.dockerenv ]; then export flavor=docker
    elif [ -f /singularity ]; then export flavor=singularity
    else export flavor=unknown; fi
    echo "[$parserDriver: enrich_version_json] adding to version.json containement : $flavor "
    jq --arg flavor $flavor '. + { "containment" : $flavor }' $BMKDIR/version.json > $baseWDir/version_derived.json
    echo -e "[$parserDriver] DEBUG: $BMKDIR/version.json\n"
    cat $BMKDIR/version.json
    echo -e "[$parserDriver] DEBUG: $baseWDir/version_derived.json\n"
    cat $baseWDir/version_derived.json
  else
      echo "[$parserDriver: enrich_version_json] $BMKDIR/version.json does not exist"
  fi
}

# Function generateSummary
# Input argument $1: status code <fail> from validateInputArguments and doOne steps:
# Input argument $2: Error message used when $1 != 0
# - <fail> < 0: validateInputArguments failed
# - <fail> > 0: doOne failed (<fail> processes failed out of $NCOPIES)
# - <fail> = 0: OK
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# The environment variable APP=<vo>-<workload> defines the name of the json file ${APP}_summary.json
function generateSummary(){
  echo "[$parserDriver:generateSummary] (reported status from calling function: $1)"
  echo "[$parserDriver:generateSummary] current directory: $(pwd)"
  s_msg="ok"
  [ "$1" -ne 0 ] && s_msg=$2
  echo "[$parserDriver:generateSummary] s_msg $s_msg"
  # Generate the json summary
  echo -e "\n[$parserDriver:generateSummary] Final report"
  local app="{}"
  if [ -f $baseWDir/version_derived.json ]; then app=$(jq . $baseWDir/version_derived.json); fi
  local OUTPUT=${APP}_summary.json
  local resJSON="{\"wl-scores\":{}}"
  if [ -f $baseWDir/parser_output.json ]; then resJSON=$(jq . $baseWDir/parser_output.json); fi
  report=$(echo $resJSON | jq --arg log "${s_msg}" '. + {"log":$log}')
  echo -e "\n[$parserDriver:generateSummary] report from output parser $report"
  echo -e "{
    \"run_info\": {\"copies\":$NCOPIES , \"threads_per_copy\":$NTHREADS , \"events_per_thread\" : $NEVENTS_THREAD },
    \"report\": ${report}, 
    \"app\":${app} }" | jq '.'> $OUTPUT
  status=$?

  echo -e "\n[$parserDriver:generateSummary] Total bmk report"
  cat $OUTPUT
  return ${status}
}

function parseResultsWrapper(){
  local statusPrecProc=$1
  s_msg="ok"
  if [ "$statusPrecProc" -ne 0 ]; then
    # if the preceeding step failed just generate the summary with that info
    echo -e "[$parserDriver:parseResultsWrapper] Run WL failed: skip parsing, go to generateSummary"
    generateSummary ${statusPrecProc} "ERROR execution"
    return 1
  else
    # otherwise run the parser
    echo -e "[$parserDriver:parseResultsWrapper] WL parse results: starting"
    echo -e "[$parserDriver:parseResultsWrapper] current directory : $(pwd)\n"
    parseResults
    subparse=$?
    echo -e "\n[$parserDriver:parseResultsWrapper] WL parser completed (status=$subparse)"
    echo -e "\n[$parserDriver:parseResultsWrapper] calling generate summary"

    generateSummary $subparse "ERROR parser" #the second argument is used only if $parse is !=0
    failedSummary=${?}
  fi
  if [ $subparse -eq 0 ] && [ $failedSummary -eq 0 ]; then
    return 0
  else
    return 1
  fi
}
