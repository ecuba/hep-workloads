HEPWL_BMKEXE=atlas-sim_mt-bmk.sh 
HEPWL_BMKOPTS="-e 3 -t 2"
HEPWL_BMKDIR=atlas-sim_mt
HEPWL_BMKDESCRIPTION="Atlas simulation MT (GEANT4) based on athena version 22.0.25"
HEPWL_DOCKERIMAGENAME=atlas-sim_mt-bmk
HEPWL_DOCKERIMAGETAG=v0.4
HEPWL_CVMFSREPOS=atlas.cern.ch,atlas-condb.cern.ch,sft.cern.ch
HEPWL_BMKOS="gitlab-registry.cern.ch/linuxsupport/cc7-base:20211101-1.x86_64"
