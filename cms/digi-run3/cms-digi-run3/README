How to generate the GlobalTag.db file
-------------------------------------

1. Set BMKDIR to the directory containing cms-digi-run3-bmk.sh

2. Copy the input root files to $BMKDIR/data

3. Find the value of the --conditions option used to generate the CMSSW
   configuration (e.g. auto:phase1_2021_realistic)

4. Look at the code of
   $CMSSW_RELEASE_BASE/src/Configuration/AlCa/python/autoCond.py
   to find the actual name of the tag (e.g. 112X_mcRun3_2021_realistic_v13)

5. Run
   $ conddb copy <tag> --destdb GlobalTag.db

6. Copy GlobalTag.db under $BMKDIR/data

7. Find and add the missing tags by running
   $ ./run_test.sh

8. Put all the conddb copy commands in generate_GlobalTag.sh

How to block connections to Frontier/squid
------------------------------------------
1. Block connections to Frontier and squid by doing
   sudo iptables -A OUTPUT -p tcp --dport 3128 -j DROP
   sudo iptables -A OUTPUT -p tcp --dport 8000 -j DROP
   sudo ip6tables -A OUTPUT -p tcp --dport 3128 -j DROP
   sudo ip6tables -A OUTPUT -p tcp --dport 8000 -j DROP
   and check that the connections to
   http://cmst0frontier.cern.ch:3128
   http://cmsfrontier.cern.ch:8000/FrontierInt
   are effectively blocked
 
2. Restore the connectivity to Frontier and squid by doing
   sudo iptables -D OUTPUT -p tcp --dport 8000 -j DROP
   sudo iptables -D OUTPUT -p tcp --dport 3128 -j DROP
   sudo ip6tables -D OUTPUT -p tcp --dport 8000 -j DROP
   sudo ip6tables -D OUTPUT -p tcp --dport 3128 -j DROP
