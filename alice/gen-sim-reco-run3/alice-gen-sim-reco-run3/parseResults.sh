#!/bin/bash

# Copyright 2019-2020 CERN. See the COPYRIGHT file at the top-level
# directory of this distribution. For licensing information, see the
# COPYING file at the top-level directory of this distribution.

parseResultsDir=$(cd $(dirname ${BASH_SOURCE}); pwd) # needed to locate parseResults.py

# Function parseResults must be defined in each benchmark (or in a separate file parseResults.sh)
# The following variables are guaranteed to be defined and exported: NCOPIES, NTHREADS, NEVENTS_THREAD, BMKDIR, DEBUG, APP
# Logfiles have been stored in process-specific working directories <basewdir>/proc_<1...NCOPIES>
# The function is started in the base working directory <basewdir>:
# please store here the overall json summary file for all NCOPIES processes combined
function parseResults(){
  #-----------------------
  # Parse results
  #-----------------------
  echo "[parseResults] parsing results from" proc_*/out_*.log
  # Parsing  Event Throughput: xxxx ev/s
  resJSON=`grep "**** Pipeline done (global_runtime :" proc_*/out_*.log | sed -e 's@.*global_runtime : \([0-9]*\.[0-9]*\)s.*@\1@' | awk -v nevents=$(($NEVENTS_THREAD * $NTHREADS)) '
    BEGIN{amin=1000000;amax=0;count=0;}
    { if ($1>0) {val=( int(nevents*1.) / int($1*1.) ); a[count]=val; count+=1; sum+=val; if(amax<val) amax=val; if(amin>val) amin=val} }
    END{sep=sprintf("%*s", 120, "");gsub(/ /, "*", sep);n=asort(a);
      if (n % 2) {
        median=a[(n + 1) / 2];
      } else {
        median=(a[(n / 2)] + a[(n / 2) + 1]) / 2.0;
      }; 
printf "\"wl-scores\": {\"gen-sim\": %.8f} , \"wl-stats\": {\"avg\": %.8f, \"median\": %.8f, \"min\": %.8f, \"max\": %.8f, \"count\": %d}",
sum, sum/count, median, amin, amax, count
    }' || (echo "\"wl-scores\":{}"; exit 1)`
  STATUS=$?
  echo "[parseResults] checking that all job are completed correctly from" proc_*/out_*.log
  #-----------------------
  # Generate summary
  #-----------------------
  echo "[parseResults] generate report"
  echo "{$resJSON}" > $baseWDir/parser_output.json
  #-----------------------
  # Return status
  #-----------------------
  # Return 0 if result parsing and json generation were successful, 1 otherwise
  return $STATUS
}
